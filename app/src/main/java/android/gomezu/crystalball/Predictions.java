package android.gomezu.crystalball;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
          "LOL Nope",
                "Hue Hue"
        };
    }

    public static Predictions get() {
        if(predictions == null) {
          predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrdictions(){
        return answers[0];
    }
}
